/*
 *  format-window.c
 *
 *  Copyright (C) 2008-2009 Red Hat, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Tomas Bzatek <tbzatek@redhat.com>
 *
 */

#ifdef HAVE_CONFIG_H
 #include <config.h> /* for GETTEXT_PACKAGE */
#endif

#include <glib/gi18n-lib.h>

#include <gdu/gdu.h>
#include <gdu-gtk/gdu-gtk.h>

#include "gdu-utils.h"
#include "format-window.h"
#include "format-window-operation.h"



/* ---------------------------------------------------------------------------------------------------- */
static void set_new_presentable (FormatDialogPrivate *priv, GduPresentable *presentable);

/* ---------------------------------------------------------------------------------------------------- */

/*  taken from palimpsest/gdu-section-unrecognized.c  */
static void
type_combo_box_changed (GtkWidget *combo_box, FormatDialogPrivate *priv)
{
  char *fstype;
  GduKnownFilesystem *kfs;
  gboolean label_entry_sensitive;
  gboolean have_owners;
  int max_label_len;

  fstype = NULL;

  label_entry_sensitive = FALSE;
  max_label_len = 0;
  have_owners = FALSE;

  fstype = gdu_util_fstype_combo_box_get_selected (combo_box);
  if (fstype != NULL && priv->presentable) {
    kfs = gdu_pool_get_known_filesystem_by_id (priv->pool, fstype);
    if (kfs != NULL) {
      max_label_len = gdu_known_filesystem_get_max_label_len (kfs);
      have_owners = gdu_known_filesystem_get_supports_unix_owners (kfs);
      g_object_unref (kfs);
    }
  }

  if (max_label_len > 0)
    label_entry_sensitive = TRUE;

  gtk_entry_set_max_length (GTK_ENTRY (priv->label_entry), max_label_len);
  gtk_widget_set_sensitive (priv->label_entry, label_entry_sensitive);

  if (have_owners)
    gtk_widget_show (priv->take_ownership_of_fs_check_button);
  else
    gtk_widget_hide (priv->take_ownership_of_fs_check_button);

  update_ui_controls (priv);

  g_free (fstype);
}


void
update_ui_controls (FormatDialogPrivate *priv)
{
  GduDevice *device = NULL;
  gboolean sensitive;

  g_return_if_fail (priv != NULL);

  if (priv->presentable && GDU_IS_PRESENTABLE (priv->presentable))
    device = gdu_presentable_get_device (priv->presentable);

  /*  mount warning box  */
  if (device && gdu_device_is_mounted (device))
    gtk_widget_show_all (priv->mount_warning);
  else
    gtk_widget_hide_all (priv->mount_warning);

  /*  read only info box  */
  if (device && gdu_device_is_read_only (device))
    gtk_widget_show_all (priv->readonly_warning);
  else
    gtk_widget_hide_all (priv->readonly_warning);

  /*  no media info box  */
  if (! device || gdu_device_is_media_available (device))
    gtk_widget_hide_all (priv->no_media_warning);
  else
    gtk_widget_show_all (priv->no_media_warning);

  /*  controls sensitivity  */
  sensitive = priv->presentable != NULL && GDU_IS_PRESENTABLE (priv->presentable) && (! priv->job_running);
  if (device)
    sensitive = sensitive && ! gdu_device_is_read_only (device) && gdu_device_is_media_available (device);

  gtk_widget_set_sensitive (priv->controls_box, sensitive);
  gtk_widget_set_sensitive (priv->partition_label, sensitive);
  if (priv->volume_selector)
    gtk_widget_set_sensitive (priv->volume_selector, ! priv->job_running);
  gtk_dialog_set_response_sensitive (priv->dialog, GTK_RESPONSE_OK, sensitive && gtk_combo_box_get_active (GTK_COMBO_BOX (priv->part_type_combo_box)) >= 0);

  if (device != NULL)
    g_object_unref (device);
}


/*  most of the code here was stolen from palimpsest  */
/*  keep in sync with gdu-shell.c/details_update()    */
static void
update_ui (FormatDialogPrivate *priv)
{
  char *s;
  char *p;
  char *name = NULL;
  GdkPixbuf *pixbuf = NULL;
  const char *usage;
  const char *type;
  const char *device_file;
  guint64 presentable_size = 0;
  char *strsize_long;
  GduDevice *device = NULL;
  GduPresentable *toplevel_presentable = NULL;
  GduDevice *toplevel_device = NULL;
  gboolean known_fs;
  GPtrArray *details;
  guint n;


  if (priv->presentable) {
    device = gdu_presentable_get_device (priv->presentable);
    toplevel_presentable = gdu_presentable_get_toplevel (priv->presentable);
    if (toplevel_presentable != NULL)
      toplevel_device = gdu_presentable_get_device (toplevel_presentable);
    name = gdu_presentable_get_name (priv->presentable);
    presentable_size = gdu_presentable_get_size (priv->presentable);
    pixbuf = gdu_util_get_pixbuf_for_presentable_at_pixel_size (priv->presentable, 112);
  }

  /*  window title  */
  s = name ? g_strdup_printf (_("Format %s"), name) : g_strdup_printf (_("Format disk"));
  gtk_window_set_title (GTK_WINDOW (priv->dialog), s);
  g_free (s);


  /*  icon  */
  gtk_image_set_from_pixbuf (GTK_IMAGE (priv->icon_image), pixbuf);
  if (pixbuf)
    g_object_unref (pixbuf);

  s = g_strdup_printf ("<span font_desc='18'><b>%s</b></span>", name ? name : "--");
  gtk_label_set_markup (GTK_LABEL (priv->name_label), s);
  g_free (s);


  usage = NULL;
  type = NULL;
  device_file = NULL;
  if (device != NULL) {
    usage = gdu_device_id_get_usage (device);
    type = gdu_device_id_get_type (device);
    device_file = gdu_device_get_device_file (device);
  }
  details = g_ptr_array_new ();


  /*  header labels  */
  if (presentable_size > 0) {
          strsize_long = gdu_util_get_size_for_display (presentable_size, TRUE);
  } else {
          strsize_long = g_strdup ("Unknown Size");
  }

  if (GDU_IS_DRIVE (priv->presentable)) {

          g_ptr_array_add (details,
                           g_strdup (strsize_long));

          if (device == NULL) {
                  /* TODO */
          } else {
                  if (gdu_device_is_removable (device)) {
                          if (gdu_device_is_partition_table (device)) {
                                  const char *scheme;
                                  scheme = gdu_device_partition_table_get_scheme (device);
                                  if (strcmp (scheme, "apm") == 0) {
                                          s = g_strdup (_("Apple Partition Map"));
                                  } else if (strcmp (scheme, "mbr") == 0) {
                                          s = g_strdup (_("Master Boot Record"));
                                  } else if (strcmp (scheme, "gpt") == 0) {
                                          s = g_strdup (_("GUID Partition Table"));
                                  } else {
                                          s = g_strdup_printf (_("Unknown Scheme: %s"), scheme);
                                  }

                                  g_ptr_array_add (details,
                                                   g_strdup_printf (_("Partitioned Media (%s)"), s));

                                  g_free (s);
                          } else if (usage != NULL && strlen (usage) > 0) {
                                  g_ptr_array_add (details,
                                                   g_strdup (_("Unpartitioned Media")));
                          } else if (!gdu_device_is_media_available (device)) {
                                  g_ptr_array_add (details,
                                                   g_strdup_printf (_("No Media Detected")));
                          } else {
                                  g_ptr_array_add (details,
                                                   g_strdup_printf (_("Unrecognized")));
                          }
                  } else {
                          if (gdu_device_is_partition_table (device)) {
                                  const char *scheme;
                                  scheme = gdu_device_partition_table_get_scheme (device);
                                  if (strcmp (scheme, "apm") == 0) {
                                          s = g_strdup (_("Apple Partition Map"));
                                  } else if (strcmp (scheme, "mbr") == 0) {
                                          s = g_strdup (_("Master Boot Record"));
                                  } else if (strcmp (scheme, "gpt") == 0) {
                                          s = g_strdup (_("GUID Partition Table"));
                                  } else {
                                          s = g_strdup_printf (_("Unknown Scheme: %s"), scheme);
                                  }
                                  g_ptr_array_add (details, s);
                          } else if (usage != NULL && strlen (usage) > 0) {
                                  g_ptr_array_add (details,
                                                   g_strdup_printf (_("Not Partitioned")));
                          } else if (!gdu_device_is_media_available (device)) {
                                  g_ptr_array_add (details,
                                                   g_strdup_printf (_("No Media Detected")));
                          } else {
                                  g_ptr_array_add (details,
                                                   g_strdup_printf (_("Unrecognized")));
                          }
                  }
          }

          if (GDU_IS_LINUX_MD_DRIVE (priv->presentable)) {
                  g_ptr_array_add (details,
                                   g_strdup (_("Linux Software RAID")));
          } else {
                  s = gdu_util_get_connection_for_display (
                          gdu_device_drive_get_connection_interface (device),
                          gdu_device_drive_get_connection_speed (device));
                  g_ptr_array_add (details,
                                   g_strdup_printf (_("Connected via %s"), s));
                  g_free (s);
          }

          if (device_file != NULL) {
                  if (gdu_device_is_read_only (device)) {
                  g_ptr_array_add (details,
                                   g_strdup_printf (_("%s (Read Only)"), device_file));
                  } else {
                  g_ptr_array_add (details,
                                   g_strdup (device_file));
                  }
          } else {
                  g_ptr_array_add (details,
                                   g_strdup (_("Not running")));
          }

  } else if (GDU_IS_VOLUME (priv->presentable)) {

          g_ptr_array_add (details,
                           g_strdup (strsize_long));

          if (strcmp (usage, "filesystem") == 0) {
                  char *fsname;
                  fsname = gdu_util_get_fstype_for_display (
                          gdu_device_id_get_type (device),
                          gdu_device_id_get_version (device),
                          TRUE);
                  g_ptr_array_add (details,
                                   g_strdup_printf (_("%s File System"), fsname));
                  g_free (fsname);
          } else if (strcmp (usage, "raid") == 0) {
                  char *fsname;
                  fsname = gdu_util_get_fstype_for_display (
                          gdu_device_id_get_type (device),
                          gdu_device_id_get_version (device),
                          TRUE);
                  g_ptr_array_add (details, fsname);
          } else if (strcmp (usage, "crypto") == 0) {
                  g_ptr_array_add (details,
                                   g_strdup (_("Encrypted LUKS Device")));
          } else if (strcmp (usage, "other") == 0) {
                  if (strcmp (type, "swap") == 0) {
                          g_ptr_array_add (details,
                                           g_strdup (_("Swap Space")));
                  } else {
                          g_ptr_array_add (details,
                                           g_strdup (_("Data")));
                  }
          } else {
                  g_ptr_array_add (details,
                                   g_strdup (_("Unrecognized")));
          }

          if (gdu_device_is_luks_cleartext (device)) {
                  g_ptr_array_add (details,
                                   g_strdup (_("Unlocked Encrypted LUKS Volume")));
          } else {
                  if (gdu_device_is_partition (device)) {
                          char *part_desc;
                          part_desc = gdu_util_get_desc_for_part_type (gdu_device_partition_get_scheme (device),
                                                                       gdu_device_partition_get_type (device));
                          g_ptr_array_add (details,
                                           g_strdup_printf (_("Partition %d (%s)"),
                                                            gdu_device_partition_get_number (device), part_desc));
                          g_free (part_desc);
                  } else {
                          g_ptr_array_add (details,
                                           g_strdup (_("Not Partitioned")));
                  }
          }

          s = g_strdup (device_file);
          if (gdu_device_is_read_only (device)) {
                  p = s;
                  s = g_strconcat (s, _(" (Read Only)"), NULL);
                  g_free (p);
          }

          if (gdu_device_is_mounted (device)) {
                  p = s;
                  s = g_strconcat (s,
                                   _(" mounted at "),
                                   gdu_device_get_mount_path (device),
                                   NULL);
                  g_free (p);
          }
          g_ptr_array_add (details, s);


  } else if (GDU_IS_VOLUME_HOLE (priv->presentable)) {

          g_ptr_array_add (details,
                           g_strdup (strsize_long));

          g_ptr_array_add (details,
                           g_strdup (_("Unallocated Space")));

          if (toplevel_device != NULL) {
                  if (gdu_device_is_read_only (toplevel_device))
                          g_ptr_array_add (details, g_strdup_printf (_("%s (Read Only)"), gdu_device_get_device_file (toplevel_device)));
                  else
                          g_ptr_array_add (details, g_strdup (gdu_device_get_device_file (toplevel_device)));
          }
  }


  if (device)
    g_debug ("gdu_device_id_get_type = '%s', gdu_device_partition_get_type = '%s'",
             gdu_device_id_get_type (device), gdu_device_partition_get_type (device));

  g_free (name);
  g_free (strsize_long);


  for (n = 0; n < 4; n++) {
          GtkWidget *label;
          const gchar *detail_str;

          switch (n) {
          case 0:
                  label = priv->details1_label;
                  break;
          case 1:
                  label = priv->details2_label;
                  break;
          case 2:
                  label = priv->details3_label;
                  break;
          case 3:
                  label = priv->details4_label;
                  break;
          }

          if (n < details->len)
                  detail_str = details->pdata[n];
          else
                  detail_str = "";

          gtk_label_set_markup (GTK_LABEL (label), detail_str);
  }

  g_ptr_array_foreach (details, (GFunc) g_free, NULL);
  g_ptr_array_free (details, TRUE);


  /*  partition type combo  */
  if (! priv->job_running) {
    known_fs = priv->presentable != NULL && type != NULL && strlen (type) > 0;
    if (priv->presentable) {
      gdu_util_fstype_combo_box_rebuild (priv->part_type_combo_box, priv->pool, NULL);
      known_fs = known_fs && gdu_util_fstype_combo_box_select (priv->part_type_combo_box, type);
    }
    if (! known_fs)
      gtk_combo_box_set_active (GTK_COMBO_BOX (priv->part_type_combo_box), -1);
  }

  /*  encryption check button  */
  if (! priv->presentable || ! gdu_pool_supports_luks_devices (priv->pool)) {
    gtk_widget_hide (priv->encrypted_check_button);
  } else {
    gtk_widget_show (priv->encrypted_check_button);
  }

  /*  Volume label  */
  if (! priv->job_running) {
    gtk_entry_set_text (GTK_ENTRY (priv->label_entry), device ? gdu_device_id_get_label (device) : "");
  }

  /*  update the partition type combo  */
  if (! priv->job_running) {
    type_combo_box_changed (priv->part_type_combo_box, priv);
  }

  if (priv->standalone_mode && priv->volume_selector != NULL) {
    gtk_widget_hide (priv->name_label);
    gtk_widget_hide (priv->icon_image);
  }

  /*  Is device mounted?  */
  update_ui_controls (priv);


  if (device != NULL)
    g_object_unref (device);

  if (toplevel_presentable != NULL)
    g_object_unref (toplevel_presentable);

  if (toplevel_device != NULL)
    g_object_unref (toplevel_device);
}


/* ---------------------------------------------------------------------------------------------------- */

static void
nautilus_gdu_destroy (FormatDialogPrivate  *priv)
{
  gboolean standalone;

  g_return_if_fail (priv != NULL);
  standalone = priv->standalone_mode;

  /*  disconnect our handlers, since the presentable (resp. the pool) refence counter doesn't really need to be zero  */
  set_new_presentable (priv, NULL);
  g_signal_handlers_disconnect_matched (priv->pool, G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, priv);

  /*  destroy the dialog and internal struct  */
  gtk_widget_destroy (GTK_WIDGET (priv->dialog));
  g_object_unref (priv->pool);
  g_free (priv);

  if (standalone)
    gtk_main_quit ();
}

static void
presentable_removed (GduPresentable *presentable, FormatDialogPrivate *priv)
{
  g_return_if_fail (priv != NULL);
  g_warning ("Presentable removed event.");

  if (priv->standalone_mode) {
    /*  we want to clear the UI, selecting another presentable might lead to accident  */
    set_new_presentable (priv, NULL);
    update_ui (priv);
    update_ui_progress (priv, NULL, priv->job_running);
  } else {
    nautilus_gdu_destroy (priv);
  }
}

static void
presentable_changed (GduPresentable *presentable, FormatDialogPrivate *priv)
{
  g_return_if_fail (priv != NULL);
  g_warning ("Presentable changed event.");

  /*  TODO: shall we preserve label or any other settings?  */
  update_ui (priv);
}

/*  we do ref presentable ourselves  */
void
select_new_presentable (FormatDialogPrivate *priv, GduPresentable *presentable)
{
  if (priv->volume_selector) {
    gdu_util_presentable_combo_box_rebuild (priv->volume_selector, priv->pool);
    gdu_util_presentable_combo_box_select (priv->volume_selector, presentable);
  }
  /*  force refresh when no standalone mode  */
  if (presentable != priv->presentable || ! priv->volume_selector)
    set_new_presentable (priv, presentable);
}

static void
set_new_presentable (FormatDialogPrivate *priv, GduPresentable *presentable)
{
  g_return_if_fail (priv != NULL);

  if (priv->presentable) {
    /*  first of all, disconnect handlers from the old presentable  */
    g_signal_handlers_disconnect_matched (priv->presentable, G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, priv);
/*    g_debug ("before unreffing presentable, count = %d [%p]", ((GObject*)priv->presentable)->ref_count, priv->presentable);  */
    g_object_unref (priv->presentable);
    priv->presentable = NULL;
  }

  if (presentable) {
    priv->presentable = g_object_ref (presentable);
/*    g_debug ("set_new_presentable: after reffing presentable, count = %d [%p]", ((GObject*)priv->presentable)->ref_count, priv->presentable);  */

    /*  catch Presentable events  */
    g_signal_connect (G_OBJECT (priv->presentable), "removed",
                      G_CALLBACK (presentable_removed), priv);
    g_signal_connect (G_OBJECT (priv->presentable), "changed",
                      G_CALLBACK (presentable_changed), priv);
  }
}

static void
pool_presentable_changed (GduPool *pool, GduPresentable *presentable, FormatDialogPrivate *priv)
{
  g_return_if_fail (priv != NULL);
  g_return_if_fail (priv->volume_selector != NULL);

  if (! priv->job_running) {
    g_warning ("Pool presentable changed event.");
    gdu_util_presentable_combo_box_rebuild (priv->volume_selector, pool);
  }
}

static void
volume_selector_combo_box_changed (GtkWidget *combo_box, FormatDialogPrivate *priv)
{
  GduPresentable *presentable;

  g_return_if_fail (priv != NULL);

  presentable = gdu_util_presentable_combo_box_get_selected (priv->volume_selector);
  if (presentable != priv->presentable) {
    set_new_presentable (priv, presentable);
    update_ui (priv);
    if (! priv->job_running)
      update_ui_progress (priv, NULL, FALSE);
  }
  if (presentable)
    g_object_unref (presentable);
}


/* ---------------------------------------------------------------------------------------------------- */

static void
cancel_operation (FormatDialogPrivate *priv)
{
  GduDevice *device;

  g_return_if_fail (priv != NULL);
  g_return_if_fail (priv->job_running == TRUE);
  /*  TODO: check for valid device  */
  g_return_if_fail (priv->presentable != NULL);
  g_warning ("Cancelling...");

  priv->job_cancelled = TRUE;
  device = gdu_presentable_get_device (priv->presentable);

  g_return_if_fail (device != NULL);
  gdu_device_op_cancel_job (device, NULL, NULL);
  g_object_unref (device);
}

static gboolean
window_delete_event (GtkWidget            *widget,
                     GdkEvent             *event,
                     FormatDialogPrivate  *priv)
{
  g_return_val_if_fail (priv != NULL, FALSE);
  if (priv->job_running) {
    cancel_operation (priv);
    return TRUE;  /*  consume the event  */
  }
  return FALSE;
}

static void
format_dialog_got_response (GtkDialog            *dialog,
                            gint                  response_id,
                            FormatDialogPrivate  *priv)
{
  if (response_id == GTK_RESPONSE_OK) {
    do_format (priv);
  }
  else {
    if (priv->job_running) {
      cancel_operation (priv);
    }
    else {
      /*  destroy the window and unref the presentable  */
      nautilus_gdu_destroy (priv);
    }
  }
}

/* ---------------------------------------------------------------------------------------------------- */

void
nautilus_gdu_spawn_dialog (GduPresentable  *presentable,
                           gboolean         standalone_mode)
{
  GtkDialog *dialog;
  GtkWidget *content_area;
  FormatDialogPrivate *priv;


  priv = g_new0 (FormatDialogPrivate, 1);
  priv->job_running = FALSE;
  priv->standalone_mode = standalone_mode;
  priv->selector_mode = presentable == NULL;
  if (presentable) {
    priv->pool = gdu_presentable_get_pool (presentable);
  } else {
    priv->pool = gdu_pool_new ();
  }

  dialog = GTK_DIALOG (gtk_dialog_new ());
  priv->dialog = dialog;

  /*  HIG stuff...  */
  gtk_dialog_set_has_separator (dialog, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
  gtk_box_set_spacing (GTK_BOX (dialog->vbox), 2);
  gtk_container_set_border_width (GTK_CONTAINER (dialog->action_area), 5);
  gtk_box_set_spacing (GTK_BOX (dialog->action_area), 6);

  gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
  gtk_window_set_title (GTK_WINDOW (dialog), "");
  /*  TODO: change icon to something more decent  */
  gtk_window_set_icon_name (GTK_WINDOW (dialog), "palimpsest");

  gtk_dialog_add_buttons (dialog,
                          _("_Format"), GTK_RESPONSE_OK,
                          NULL);
  priv->close_button = gtk_dialog_add_button (dialog, GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
  gtk_dialog_set_default_response (dialog, GTK_RESPONSE_CLOSE);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
  gtk_container_set_border_width (GTK_CONTAINER (content_area), 10);


  /*  headlines  */
  GtkWidget *label;
  GtkWidget *align;
  GtkWidget *vbox3;
  GtkWidget *vbox2;
  GtkWidget *hbox;
  GtkWidget *image;
  GtkWidget *table;
  GtkWidget *entry;
  GtkWidget *combo_box;
  GtkWidget *check_button;
  GtkWidget *progress_bar;
  int row;


  /*  Selector  */
  if (standalone_mode && presentable == NULL) {
    priv->volume_selector = gdu_util_presentable_combo_box_create (priv->pool);
    gtk_box_pack_start (GTK_BOX (content_area), priv->volume_selector, TRUE, TRUE, 0);
  }

  hbox = gtk_hbox_new (FALSE, 10);
  gtk_box_pack_start (GTK_BOX (content_area), hbox, TRUE, TRUE, 0);

  image = gtk_image_new ();
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
  priv->icon_image = image;

  vbox3 = gtk_vbox_new (FALSE, 0);
  align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 0, 20*(priv->volume_selector != NULL), 25*(priv->volume_selector != NULL), 0);
  gtk_container_add (GTK_CONTAINER (align), vbox3);
  gtk_box_pack_start (GTK_BOX (hbox), align, TRUE, TRUE, 0);

  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, TRUE, 0);
  priv->name_label = label;

  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, TRUE, 0);
  priv->details1_label = label;

  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, TRUE, 0);
  priv->details2_label = label;

  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, TRUE, 0);
  priv->details3_label = label;

  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, TRUE, 0);
  priv->details4_label = label;


  /*  partition  */
  vbox3 = gtk_vbox_new (FALSE, 2);
  align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 10, 30, 24, 10);
  gtk_container_add (GTK_CONTAINER (align), vbox3);
  gtk_box_pack_start (GTK_BOX (content_area), align, FALSE, TRUE, 0);

  label = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (label), _("<b>Format</b>"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, FALSE, 0);
  priv->partition_label = label;

  vbox2 = gtk_vbox_new (FALSE, 5);
  align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 0, 0, 24, 0);
  gtk_container_add (GTK_CONTAINER (align), vbox2);
  gtk_box_pack_start (GTK_BOX (vbox3), align, FALSE, TRUE, 0);
  priv->controls_box = vbox2;

  /* explanatory text */
  label = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (label), _("To create a new file system on the device, select the type "
                                             "and label and then press \"Format\". All existing data will "
                                             "be lost."));
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, TRUE, 0);

  row = 0;

  table = gtk_table_new (2, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox2), table, FALSE, FALSE, 0);

  /*  partition label  */
  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_label_set_markup_with_mnemonic (GTK_LABEL (label), _("_Label:"));
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, row, row + 1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), entry, 1, 2, row, row + 1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  gtk_label_set_mnemonic_widget (GTK_LABEL (label), entry);
  priv->label_entry = entry;
  row++;

  /*  partition type  */
  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_label_set_markup_with_mnemonic (GTK_LABEL (label), _("_Type:"));
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, row, row + 1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  combo_box = gdu_util_fstype_combo_box_create (priv->pool, NULL);
  gtk_table_attach (GTK_TABLE (table), combo_box, 1, 2, row, row +1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  gtk_label_set_mnemonic_widget (GTK_LABEL (label), combo_box);
  priv->part_type_combo_box = combo_box;
  row++;

  /* type desc */
  label = gtk_label_new (NULL);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_label_set_width_chars (GTK_LABEL (label), 40);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE (table), label, 1, 2, row, row + 1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  gdu_util_fstype_combo_box_set_desc_label (combo_box, label);
  row++;

  /* whether to chown fs root for user */
  check_button = gtk_check_button_new_with_mnemonic (_("T_ake ownership of file system"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_button), TRUE);
  gtk_widget_set_tooltip_text (check_button,
                               _("The selected file system has a concept of file ownership. "
                                 "If checked, the created file system be will be owned by you. "
                                 "If not checked, only the super user can access the file system."));
  gtk_table_attach (GTK_TABLE (table), check_button, 1, 2, row, row + 1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  priv->take_ownership_of_fs_check_button = check_button;
  row++;

  /* whether to encrypt underlying device */
  check_button = gtk_check_button_new_with_mnemonic (_("E_ncrypt underlying device"));
  gtk_widget_set_tooltip_text (check_button,
                               _("Encryption protects your data, requiring a "
                                 "passphrase to be enterered before the file system can be "
                                 "used. May decrease performance and may not be compatible if "
                                 "you use the media on other operating systems."));
  gtk_table_attach (GTK_TABLE (table), check_button, 1, 2, row, row + 1,
                    GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 2);
  priv->encrypted_check_button = check_button;
  row++;

  /*  progress bar  */
  align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 15, 0, 20, 25);
  priv->progress_bar_box = align;

  progress_bar = gtk_progress_bar_new ();
  priv->progress_bar = progress_bar;
  gtk_container_add (GTK_CONTAINER (align), progress_bar);
  gtk_box_pack_start (GTK_BOX (vbox3), align, TRUE, TRUE, 0);


  /*  mounted warning box  */
  hbox = gtk_hbox_new (FALSE, 7);
  align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 15, 15, 20, 0);
  gtk_container_add (GTK_CONTAINER (align), hbox);
  gtk_box_pack_start (GTK_BOX (content_area), align, FALSE, TRUE, 0);
  priv->mount_warning = align;
  image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_WARNING, GTK_ICON_SIZE_LARGE_TOOLBAR);
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
  label = gtk_label_new (_("The volume is currently mounted. Please make sure to close all open files before formatting."));
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);


  /*  readonly info box  */
  hbox = gtk_hbox_new (FALSE, 7);
  align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 10, 10, 20, 0);
  gtk_container_add (GTK_CONTAINER (align), hbox);
  gtk_box_pack_start (GTK_BOX (content_area), align, FALSE, TRUE, 0);
  priv->readonly_warning = align;
  image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_LARGE_TOOLBAR);
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
  label = gtk_label_new (_("Device is read only"));
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);


  /*  no media info box  */
  hbox = gtk_hbox_new (FALSE, 7);
  align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (align), 10, 10, 20, 0);
  gtk_container_add (GTK_CONTAINER (align), hbox);
  gtk_box_pack_start (GTK_BOX (content_area), align, FALSE, TRUE, 0);
  priv->no_media_warning = align;
  image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_LARGE_TOOLBAR);
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
  label = gtk_label_new (_("No media in drive"));
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);


  g_signal_connect (priv->dialog, "delete-event",
                    G_CALLBACK (window_delete_event), priv);
  /*  update sensivity and length of fs label + entry  */
  g_signal_connect (priv->part_type_combo_box, "changed",
                    G_CALLBACK (type_combo_box_changed), priv);
  g_signal_connect (G_OBJECT (dialog), "response",
                    G_CALLBACK (format_dialog_got_response), priv);
  /*  GduPool signals in standalone mode  */
  if (standalone_mode && presentable == NULL && priv->volume_selector != NULL) {
    g_signal_connect (priv->pool, "presentable-added", G_CALLBACK (pool_presentable_changed), priv);
    g_signal_connect (priv->pool, "presentable-changed", G_CALLBACK (pool_presentable_changed), priv);
    g_signal_connect (priv->pool, "presentable-removed", G_CALLBACK (pool_presentable_changed), priv);
    g_signal_connect (priv->volume_selector, "changed", G_CALLBACK (volume_selector_combo_box_changed), priv);
  }


  gtk_widget_show_all (GTK_WIDGET (dialog));
  gtk_widget_grab_focus (priv->close_button);
  set_new_presentable (priv, presentable);
  update_ui (priv);
  update_ui_progress (priv, NULL, FALSE);
}
