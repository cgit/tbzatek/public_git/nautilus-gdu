/*
 *  nautilus-gdu.c
 *
 *  Copyright (C) 2008-2009 Red Hat, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Tomas Bzatek <tbzatek@redhat.com>
 *
 */

#ifdef HAVE_CONFIG_H
 #include <config.h> /* for GETTEXT_PACKAGE */
#endif

#include "nautilus-gdu.h"
#include "format-window.h"
#include "gdu-utils.h"

#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <gdu/gdu.h>



static void nautilus_gdu_instance_init (NautilusGDU      *cvs);
static void nautilus_gdu_class_init    (NautilusGDUClass *class);


static GType nautilus_gdu_type = 0;

/*  TODO: push upstream  */
#define G_FILE_ATTRIBUTE_MOUNTABLE_UNIX_DEVICE_FILE   "mountable::unix-device-file"



/*  caller must unref the returned object  */
static GduPresentable *
find_presentable_from_file (NautilusFileInfo *nautilus_file)
{
  GduPresentable *presentable = NULL;
  GFile *file;
  GFileInfo *info;
  GError *error;
  GFileType file_type;
  GMount *mount;
  GVolume *volume;
  char *mount_path = NULL;
  char *device_file = NULL;

  g_return_val_if_fail (nautilus_file != NULL, NULL);
  file = nautilus_file_info_get_location (nautilus_file);
  g_return_val_if_fail (file != NULL, NULL);
  file_type = nautilus_file_info_get_file_type (nautilus_file);

  /*  first try to find mount target from a mountable  */
  if (file_type == G_FILE_TYPE_MOUNTABLE || file_type == G_FILE_TYPE_SHORTCUT) {
    /*  get a mount if exists and extract device file from it  */
    mount = nautilus_file_info_get_mount (nautilus_file);
    if (mount) {
      mount_path = nautilus_file_info_get_activation_uri (nautilus_file);
      volume = g_mount_get_volume (mount);
      if (volume) {
        device_file = g_volume_get_identifier (volume, "unix-device");
        g_object_unref (volume);
      }
      g_object_unref (mount);
    }

    /*  not mounted, assuming we've been spawned from computer://  */
    if (mount_path == NULL && device_file == NULL) {
      error = NULL;
      /*  retrieve DeviceKit device ID for non-mounted devices  */
      info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_TARGET_URI "," G_FILE_ATTRIBUTE_MOUNTABLE_UNIX_DEVICE_FILE, G_FILE_QUERY_INFO_NONE, NULL, &error);
      if (info) {
        device_file = g_file_info_get_attribute_as_string (info, G_FILE_ATTRIBUTE_MOUNTABLE_UNIX_DEVICE_FILE);
        mount_path = g_file_info_get_attribute_as_string (info, G_FILE_ATTRIBUTE_STANDARD_TARGET_URI);
        g_object_unref (info);
      }
      if (error) {
        g_warning ("unable to query info: %s\n", error->message);
        g_clear_error (&error);
      }
    }
  }

  /*  try to guess mount from a path (e.g. /media/disk)  */
  if (mount_path == NULL && file_type == G_FILE_TYPE_DIRECTORY) {
    error = NULL;
    info = g_file_query_info (file, G_FILE_ATTRIBUTE_UNIX_IS_MOUNTPOINT, G_FILE_QUERY_INFO_NONE, NULL, &error);
    if (info) {
      if (g_file_info_get_attribute_boolean (info, G_FILE_ATTRIBUTE_UNIX_IS_MOUNTPOINT))
        mount_path = g_file_get_path (file);
      g_object_unref (info);
    }
    if (error) {
      g_warning ("unable to query info: %s\n", error->message);
      g_clear_error (&error);
    }
  }

  g_print ("nautilus-gdu: matching device_file = '%s', mount_path = '%s'\n", device_file, mount_path);

  if (device_file)
    presentable = find_presentable_from_device_path (device_file);
  else
  if (mount_path)
    presentable = find_presentable_from_mount_path (mount_path);

  g_free (device_file);
  g_free (mount_path);
  g_object_unref (file);

  if (presentable)
    g_print ("nautilus-gdu: find_presentable_from_file: found presentable '%s'\n", gdu_presentable_get_name (presentable));
  else
    g_print ("nautilus-gdu: find_presentable_from_file: no presentable found\n");

  return presentable;
}


static void
format_callback (NautilusMenuItem *item,
                 gpointer user_data)
{
  GduPresentable *presentable;

  presentable = g_object_get_data (G_OBJECT (item), "presentable");
  g_return_if_fail (GDU_IS_PRESENTABLE (presentable));
  nautilus_gdu_spawn_dialog (presentable, FALSE);
}

GList *
nautilus_gdu_get_file_items (NautilusMenuProvider *provider,
			     GtkWidget            *window,
			     GList                *files)
{
  NautilusMenuItem *item;
  GduPresentable *presentable;

  if (g_list_length (files) != 1) {
    return NULL;
  }

  presentable = find_presentable_from_file (files->data);
  if (! presentable)
    return NULL;

  /*  TODO: icon  */
  item = nautilus_menu_item_new ("NautilusGDU::format",
                                 _("_Format..."),
                                 _("Create new filesystem on the selected device"),
                                 "palimpsest");
  g_object_set_data (G_OBJECT (item),
                     "NautilusGDU::screen",
                     gtk_widget_get_screen (window));
  g_object_set_data_full (G_OBJECT (item), "presentable",
                          g_object_ref (presentable),
                          (GDestroyNotify) g_object_unref);
  g_signal_connect (item, "activate",
                    G_CALLBACK (format_callback),
                    NULL);

  g_object_unref (presentable);

  return g_list_append (NULL, item);
}


#if 0
GList *
nautilus_gdu_get_toolbar_items (NautilusMenuProvider *provider,
				GtkWidget            *window,
				NautilusFileInfo     *current_folder)
{
  g_print ("nautilus_gdu_get_toolbar_items\n");
  /*  not used  */
  return NULL;
}

static GList *
nautilus_gdu_get_background_items (NautilusMenuProvider *provider,
                                   GtkWidget            *window,
                                   NautilusFileInfo     *file_info)
{
  g_print ("nautilus_gdu_get_background_items\n");
  /*  not used  */
  return NULL;
}
#endif

static void
nautilus_gdu_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
  iface->get_file_items = nautilus_gdu_get_file_items;
#if 0
  iface->get_background_items = nautilus_gdu_get_background_items;
  iface->get_toolbar_items = nautilus_gdu_get_toolbar_items;
#endif
}

static void
nautilus_gdu_instance_init (NautilusGDU *cvs)
{
}

static void
nautilus_gdu_class_init (NautilusGDUClass *class)
{
}

GType
nautilus_gdu_get_type (void)
{
  return nautilus_gdu_type;
}

void
nautilus_gdu_register_type (GTypeModule *module)
{
  static const GTypeInfo info = {
    sizeof (NautilusGDUClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) nautilus_gdu_class_init,
    NULL,
    NULL,
    sizeof (NautilusGDU),
    0,
    (GInstanceInitFunc) nautilus_gdu_instance_init,
  };

  static const GInterfaceInfo menu_provider_iface_info = {
    (GInterfaceInitFunc) nautilus_gdu_menu_provider_iface_init,
    NULL,
    NULL
  };

  nautilus_gdu_type = g_type_module_register_type (module,
                                                   G_TYPE_OBJECT,
                                                   "NautilusGDU",
                                                   &info, 0);

  g_type_module_add_interface (module,
                               nautilus_gdu_type,
                               NAUTILUS_TYPE_MENU_PROVIDER,
                               &menu_provider_iface_info);
}

